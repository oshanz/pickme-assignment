<html>

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
		crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
		crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
		crossorigin="anonymous"></script>

	<script src="static/js/centrifuge.min.js" type="text/javascript"></script>

	<script type="text/javascript">

		var centrifuge = new Centrifuge({
			url: 'http://localhost:8000',
			user: "1",
			timestamp: "{{.timestamp}}",
			token: "{{.token}}"
		});

		centrifuge.subscribe("news", function(message) {
			console.log(message);
		});

		centrifuge.connect();

	</script>
</head>

<body>

	<h3>Driver App</h3>

	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<input readonly="readonly" id="iam" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<div id="msg">msg</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<button id="on" class="btn btn-default">On</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<button id="off" class="btn btn-default">Off</button>
		</div>
	</div>

</body>

</html>
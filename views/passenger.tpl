<html>

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
		crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
		crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
		crossorigin="anonymous"></script>

	<script src="static/js/centrifuge.min.js" type="text/javascript"></script>

	<script type="text/javascript">

		var centrifuge = new Centrifuge({
			url: 'http://localhost:8000',
			user: "2",
			timestamp: "{{.timestamp}}",
			token: "{{.token}}"
		});

		centrifuge.subscribe("news", function(message) {
			console.log(message);
		});

		centrifuge.connect();

		$(function(){
			$('#call_driver').click(function(){
				$(this).prop("disabled",true);
				$(this).removeClass('btn-default');
				$(this).addClass('btn-warning');
				$(this).html('Hiling');
				notifyDriver();
			});
		});

		function notifyDriver(){
			$.post('http://localhost:8080/driver/1/hail', function(data){
				console.log(data);
			});
		}

	</script>
</head>

<body>

	<h3>Passenger App</h3>

	<table class="table">
<tr>
<td>ID: 1 | Name: Driver 1</td>
<td><button class="btn btn-default" id="call_driver">Hail</button></td>
</tr>
	</table>

</body>

</html>
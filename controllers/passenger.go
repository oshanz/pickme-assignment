package controllers

import (
	"fmt"
	"time"

	"github.com/astaxie/beego"
	"github.com/centrifugal/centrifugo/libcentrifugo/auth"
)

// PassengerController operations for Passenger
type PassengerController struct {
	beego.Controller
}

// URLMapping ...
func (c *PassengerController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Get", c.Get)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

func (c *PassengerController) Get() {
	secret := "8209cd29-7ba0-4bdf-b2a0-f06bcb22b5ec"
	timestamp := fmt.Sprintf("%v", time.Now().Unix())
	token := auth.GenerateClientToken(secret, "2", timestamp, "")
	c.Data["token"] = token
	c.Data["timestamp"] = timestamp
	c.TplName = "passenger.tpl"
}

// Post ...
// @Title Create
// @Description create Passenger
// @Param	body		body 	models.Passenger	true		"body for Passenger content"
// @Success 201 {object} models.Passenger
// @Failure 403 body is empty
// @router / [post]
func (c *PassengerController) Post() {

}

// GetOne ...
// @Title GetOne
// @Description get Passenger by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Passenger
// @Failure 403 :id is empty
// @router /:id [get]
func (c *PassengerController) GetOne() {

}

// GetAll ...
// @Title GetAll
// @Description get Passenger
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Passenger
// @Failure 403
// @router / [get]
func (c *PassengerController) GetAll() {

}

// Put ...
// @Title Put
// @Description update the Passenger
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Passenger	true		"body for Passenger content"
// @Success 200 {object} models.Passenger
// @Failure 403 :id is not int
// @router /:id [put]
func (c *PassengerController) Put() {

}

// Delete ...
// @Title Delete
// @Description delete the Passenger
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *PassengerController) Delete() {

}

package controllers

import (
	"fmt"
	"time"

	"github.com/astaxie/beego"
	"github.com/centrifugal/centrifugo/libcentrifugo/auth"
	"github.com/centrifugal/gocent"
)

// DriverController operations for Driver
type DriverController struct {
	beego.Controller
}

// URLMapping ...
func (c *DriverController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("Get", c.Get)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

func (c *DriverController) Get() {
	secret := "8209cd29-7ba0-4bdf-b2a0-f06bcb22b5ec"
	client := gocent.NewClient("http://localhost:8000", secret, 5*time.Second)
	ok, err := client.Publish("news", []byte(`{"input": "test"}`))
	if err != nil {
		println(err.Error())
		return
	}
	println("news publish", ok)
	channels, _ := client.Channels()
	fmt.Printf("Channels: %v\n", channels)
	timestamp := fmt.Sprintf("%v", time.Now().Unix())
	token := auth.GenerateClientToken(secret, "1", timestamp, "")
	c.Data["token"] = token
	c.Data["timestamp"] = timestamp
	c.TplName = "driver.tpl"
}

// Post ...
// @Title Create
// @Description create Driver
// @Param	body		body 	models.Driver	true		"body for Driver content"
// @Success 201 {object} models.Driver
// @Failure 403 body is empty
// @router / [post]
func (c *DriverController) Post() {

}

// GetOne ...
// @Title GetOne
// @Description get Driver by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Driver
// @Failure 403 :id is empty
// @router /:id [get]
func (c *DriverController) GetOne() {

}

// GetAll ...
// @Title GetAll
// @Description get Driver
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Driver
// @Failure 403
// @router / [get]
func (c *DriverController) GetAll() {

}

// Put ...
// @Title Put
// @Description update the Driver
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Driver	true		"body for Driver content"
// @Success 200 {object} models.Driver
// @Failure 403 :id is not int
// @router /:id [put]
func (c *DriverController) Put() {

}

// Delete ...
// @Title Delete
// @Description delete the Driver
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *DriverController) Delete() {

}

package main

import (
	_ "pickme/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}


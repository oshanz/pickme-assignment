package routers

import (
	"pickme/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/driver", &controllers.DriverController{})
	beego.Router("/passenger", &controllers.PassengerController{})
}

## Dependencies ##

* go v1.7.3	
* beego
* centrifugo v1.5.1
* Gocent
* go-sqlite3

## Roadmap ##

version 1

* sqlite database
* beego web framework
* push notification from centrifugo


## Todo ##

version 2

* tweak ui
* use database
* increase API security